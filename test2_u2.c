#include<stdio.h>
#include<stdlib.h>
#include<string.h>

  // typ pro ulozeni polozky
typedef struct Polozka {
  char jmeno;  // jmeno i prijmeni alokujeme dynamicky
  unsigned int vek;
  struct Polozka *nasl;  // naslednik a predchudce pro vytvoreni dvousmerneho seznamu
} TPolozka;

  // funkce pro vytvoreni nove polozky
TPolozka*novaPolozka(char*j, int v, TPolozka *head) {
  TPolozka *result = (TPolozka*)malloc(sizeof(TPolozka));  // nova polozka
  result->jmeno = (char*)malloc(strlen(j)+1);    // alokujeme jmeno odpovidajici delky

  strcpy(result->jmeno, j);        // nakopirujeme jmeno
  result->vek = v;          // zkopirujeme vek

  if (head == NULL) {        // nemame-li predka
    result->nasl = result;        // i naslednikem my
  } else {          // mame-li predka
    result->nasl = head->nasl;       // nas naslednik je naslednik puvodniho head
    head->nasl = result;             // naslednik naseho predchudce jsme my
  }

  return result;            // vratime novou polozku
}

  /*
void odstranPolozku(TPolozka **p) {
  if (p != NULL && *p != NULL) {      // obdrzeli jsme neco, co neni NULL?
    (*p)->pred->nasl = (*p)->nasl;      // naseho predchudce naslednik je nas naslednik
    (*p)->nasl->pred = (*p)->pred;      // naseho naslednika predchudce je nas predchudce

    free ((*p)->jmeno);        // uvolnime jmeno
    free ((*p)->prijmeni);        // uvolnime prijmeni
    free (*p);          // uvolnime vlastni polozku
    *p = NULL;          // a nastavime ukazatel na NULL, aby nebyl omylem pouzit
  }
}

  // funkce pro vypis polozek od nejnovejsi po nejstarsi
void vypisPolozky(TPolozka *head) {
  TPolozka *i = head;

  if (i != NULL) {        // pokud nedrzime NULL
    do {            // opakujeme vypis (pohlavi nevypisujeme)
      printf("%s\n%s\n%d\n\n", i->jmeno, i->prijmeni, i->vek);
      i = i->pred;
    } while (i != head);        // dokud se nedostaneme zase k hlave
  }
}
*/
  // vlastni funkce main
int main (void) {
  TPolozka *head, *tmp = NULL;          // deklarace hlavy a pomocneho ukazatele
  FILE *f;
  if(f = fopen("data.txt", "r") == NULL){
    printf("Nepodarilo se otevrit soubor, koncim");
    fclose(f);
    return(EXIT_FAILURE);
  }
  char jmeno[20];
  unsigned int vek;

  while(fscanf("%s", &jmeno) != EOF){
    while(fscanf("%d", &vek) == 1){
      head = tmp;
      tmp = novaPolozka(jmeno, vek, head);
    }
  }


  return EXIT_SUCCESS;
}

